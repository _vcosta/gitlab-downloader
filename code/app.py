import requests
from sh import git, mv
import os

# Credentials
BASE_GROUP = ""
PRIVATE_TOKEN = ""

ROOT_DIR = "/root/dev/code/repos/"

def get_subgroups(group_id, tab, parent=None):
    ligature = '└─' if tab != 0 else ''
    tab += 1
    r = requests.get(f"https://gitlab.com/api/v4/groups/{group_id}/subgroups", 
                    headers={"PRIVATE-TOKEN":PRIVATE_TOKEN}).json()
    parent_dir = f'{parent}/' if parent else ''
    for item in r:
        print("{:15d} {}".format(item['id'], tab * '   ' + ligature + '\033[1;31m' + item['name'] + '\033[0;0m' + f" : {parent_dir}{item['name']}"), flush=True)
        os.makedirs(f"{ROOT_DIR}{parent_dir}{item['name']}", exist_ok=True)
        get_subgroups(str(item['id']), tab, parent_dir + item['name'])
        get_repos(item['id'], tab, parent_dir + item['name'])


def get_repos(group_id, tab, parent=None):
    ligature = '└─' if tab != 0 else ' '
    r = requests.get(f"https://gitlab.com/api/v4/groups/{group_id}", 
                    headers={"PRIVATE-TOKEN":PRIVATE_TOKEN}).json()    
    for item in r['projects']:
        print("{:15d}   {:40s}  {}".format(item['id'], tab * '   ' + ligature + "\033[0;0m" + item['name'], item['ssh_url_to_repo']), flush=True)
        try:
            git("clone", item['ssh_url_to_repo'])
        except:
            print('\033[33m' + f"{60*' '}Error cloning repo: {item['name']}" + '\033[0;0m', flush=True)
            continue
        parent_dir = f'{parent}/' if parent else ''
        # os.makedirs(f"{ROOT_DIR}{parent_dir}{item['name']}", exist_ok=True)
        try:
            mv(f"{item['name']}", f"{ROOT_DIR}{parent_dir}")
        except Exception as e:
            print('\033[33m' + f"{60*' '}Error moving repo: {item['name']}" + '\033[0;0m', flush=True)

get_subgroups(BASE_GROUP, 0)
get_repos(BASE_GROUP, 0)
