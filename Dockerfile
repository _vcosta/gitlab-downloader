FROM python:3
MAINTAINER Vinicius "vinicius.costa@semantix.com.br"
LABEL Version="0.1"

# RUN apk --update --no-cache add py-pip
RUN pip install requests \
                sh

WORKDIR /root/dev

CMD ["python"]