# Boto3 - AWS Resource listing

## Configuring
Set credential variables inside **app.py**

> BASE_GROUP = "Group number on GitLab"
> PRIVATE_TOKEN = "Access token for API with read repository permissions"

On docker-compose.yml file, set $HOME with the configured **.ssh** key to be able to download private repositories.

## Running

> docker-compose up

All results are saved in the **code/repos** folder.

ps. Repos ownership must be set for the current user.